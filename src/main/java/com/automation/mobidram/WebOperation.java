package com.automation.mobidram;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

public class WebOperation {
	WebDriver driver = null;
	
	protected String username = null;
	protected String password = null;
	protected String accountNumber = null;
	protected String accountOwner = null;
	protected String comment = null;
	protected String transferAmount = null;
	protected String pin = null;
	protected String refillAmount = null;
	protected String bankAccount = null;
	protected String payment = null;
	
	@Parameters({ "username", "password", "accountNumber", "accountOwner", "comment", "transferAmount", "pin", "refillAmount", "bankAccount", "payment" })
	@BeforeTest
	public void setParams(String username, String password, String accountNumber,String accountOwner, String comment, String transferAmount, String pin, String refillAmount, String bankAccount, String payment) {
		this.username = username;
		this.password = password;
		this.accountNumber = accountNumber;
		this.accountOwner = accountOwner;
		this.comment = comment;
		this.transferAmount = transferAmount;
		this.pin = pin;
		this.refillAmount = refillAmount;
		this.bankAccount = bankAccount;
		this.payment = payment;
	}
	
	// Set Driver
	public void SetDriver(String  browser) {
		DesiredCapabilities capabilities = new DesiredCapabilities();
		String osName = System.getProperty("os.name").toLowerCase();
		if (browser == "Chrome") {
			capabilities.setCapability("browser", "Chrome");
			if (osName.contains("win")) {
				System.setProperty("webdriver.chrome.driver","browserDriver/chromedriver.exe");
			} else {
				System.setProperty("webdriver.chrome.driver","browserDriver/chromedriver");
			}
			driver = new ChromeDriver(); 
		}
	}
	
	// Login Mobi Dram account
	public void login() {
		//Open Mobi Dram
		driver.get("https://login.mobidram.am");
		
		// Login to Mobi Dram
		WebDriverWait wait = new WebDriverWait(driver, 90);
		
		WebElement uname = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("ctl00_DefaultContent_txtUserName")));
		uname.sendKeys(username);
		WebElement pass = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("ctl00_DefaultContent_txtPassword")));
		pass.sendKeys(password);
		WebElement login = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("ctl00_DefaultContent_rbtLogin_input")));
		login.click();
	}
	
	//Go to Transfer
	public void trnasfer() {
		WebElement elm = (new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"ctl00_navigationFirstLevel\"]/ul/li[5]/a/span"))));
		elm.click();
	}
	
	//Go to Account Refill
	public void accountRefill() {
		WebElement elm = (new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"ctl00_navigationFirstLevel\"]/ul/li[2]/a/span"))));
		elm.click();
	}
		
	//Go to Transfer Bank Account
	public void transferToBankAccount() {
		WebElement elm = (new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"ctl00_navigationSecondLevel\"]/ul/li[3]/a/span"))));
		elm.click();
	}
	
	//Select Bank
	public void selectBank() {
		driver.findElement(By.id("ctl00_DefaultContent_ucBank_cmbLoanOrganisation_Arrow")).click();
		WebElement elm = (new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[text() = '"+ bankAccount +"']"))));
		elm.click();
	}
	
	//Select Bank Account
		public void selectBankAccount() {
			driver.findElement(By.id("ctl00_DefaultContent_ddlBankAccounts_Arrow")).click();
			WebElement elm = (new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[text() = '"+ bankAccount +"']"))));
			elm.click();
		}
	
	//Entert Bank Account Data
	public void entertBankAccountData() {
		WebDriverWait wait = new WebDriverWait(driver, 90);
		
		//Enter Account Number
		WebElement number = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("ctl00_DefaultContent_ucBankTransferControl_txtReceiverAccountNumber")));
		number.sendKeys(accountNumber);
		
		//Enter Account Owner
		WebElement owner = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("ctl00_DefaultContent_ucBankTransferControl_txtReceiverName")));
		owner.sendKeys(accountOwner);
		
		//Enter Comment
		WebElement com = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("ctl00_DefaultContent_ucBankTransferControl_txtAgreementNumber")));
		com.sendKeys(comment);
	}
	
	//Enter Refill Amount
	public void enterRefillAmount() {
		WebElement elm = (new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(By.id("ctl00_DefaultContent_ntbAmount"))));
		elm.sendKeys(refillAmount);
	}
		
	//Enter Transfer Amount
	public void enterTransferAmount() {
		WebElement elm = (new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(By.id("ctl00_DefaultContent_ucBankTransferControl_ntbAmount"))));
		elm.sendKeys(transferAmount);
	}
	
	//Select Payment Channel
	public void selectPaymentChannel() {
		driver.findElement(By.id("ctl00_DefaultContent_ucBankTransferControl_ucPaymentMethod_cmbPaymentMethod_Arrow")).click();
		//WebElement elm = (new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(By.id("ctl00_DefaultContent_ucBankTransferControl_ucPaymentMethod_cmbPaymentMethod_i1_pnlTemplate"))));
		WebElement elm = (new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[text() = '"+ payment +"']"))));
		elm.click();
	}
	
	//Refill Calculate
	public void refillCalculate() {
		WebElement elm = (new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(By.id("ctl00_DefaultContent_rbtCalculate_input"))));
		elm.click();
	}
		
	//Transfer Calculate
	public void transferCalculate() {
		WebElement elm = (new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(By.id("ctl00_DefaultContent_ucBankTransferControl_rbtCalculate"))));
		elm.click();
	}
			
	//Enter Pin
	public void enterPin() {
		WebElement elm = (new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(By.id("ctl00_DefaultContent_ucBankTransferControl_txtPin"))));
		elm.sendKeys(pin);
	}
	
	//Confirm Refill
	public void confirmRefill() {
		WebElement elm = (new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(By.id("ctl00_DefaultContent_rbtConfirm_input"))));
		elm.click();
		System.out.println("Refiill Account:" + refillAmount);
	}
	
	//Confirm Transfer
	public void confirmTransfer() {
		WebElement elm = (new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(By.id("ctl00_DefaultContent_ucBankTransferControl_rbtConfirm"))));
		elm.click();
		System.out.println("Transfer Bank Account:" + transferAmount);
	
	}
	
	//Second Confirm 
	public void secondConfirm() throws InterruptedException {	  
		WebElement elm = (new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Yes"))));
		elm.click();	
		System.out.println("Withdraw Bank Account:" + transferAmount);		
	}
	
	// Quit driver and close browser
	public void quit() {
		driver.quit();		
	}
	
	public int getRandomIntegerBetweenRange(int min, int max){
	    int x = (int)(Math.random()*((max-min)+1))+min;
	    return x;
	}
}
