package com.automation.mobidram;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TransferBankAccountTest extends WebOperation {
		WebDriver driver = null;
	    
		@BeforeClass
		public void beforeClass() throws Exception {
			SetDriver("Chrome");
		}

		@Test(priority=0)
		public void Login() {
			//Log In
			login();
		}
		
		@Test(priority=1, invocationCount = 1)
		public void Transfer() throws InterruptedException {
			
			//Transfer
			trnasfer();
			transferToBankAccount();
			selectBankAccount();
			enterTransferAmount();
			transferCalculate();
			enterPin();
			confirmTransfer();
			
			//Sleep 
			int time = getRandomIntegerBetweenRange(5,10);
			TimeUnit.SECONDS.sleep(time);
	
		}
		
		@Test(priority=2)
		public void Quit() {
			//Quit
			quit();
		}
	}
