package com.automation.mobidram;

import org.testng.annotations.Test;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;

@Test
public class WithDrawTest extends WebOperation {
	WebDriver driver = null;
	
	@BeforeTest
	public void beforeTest() throws Exception {
		SetDriver("Chrome");
	}
	
	@Test(priority=0)
	public void Login() {
		login();
	}
	
	@Test(priority=1, invocationCount = 2,  threadPoolSize = 1)
	public void Transaction() throws InterruptedException {
		trnasfer();
		transferToBankAccount();
		selectBankAccount();
		enterTransferAmount();
		selectPaymentChannel();
		transferCalculate();
		enterPin();
		confirmTransfer();
		//secondConfirm();
		
		//Sleep 
		//int time = getRandomIntegerBetweenRange(40,60);
		//TimeUnit.SECONDS.sleep(time);
	}
	
	@AfterTest
	public void afterTest() {
		quit();
	}
}
